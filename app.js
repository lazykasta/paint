import { createStore, combineReducers } from 'redux';
import { Map } from 'immutable'; // Used to assure immutability of states
import expect from 'expect';

//Initial states
const initialDraw = Map({
    color: '#000',
    painting:false,
    size: 5,
    offset: [],
    coord: [],
    clear: false,
    undo: false
});

const initialSettings = Map({
    color: {
        value: '#000',
        id: 'black'
    },
    size: {
        value: 5,
        id: 'normal'
    },
    style: {
        value: 'round',
        id: 'round'
    }
});

//reducers
const draw = (state=initialDraw, action={}) => {
    switch(action.type) {
        case 'MOUSEDOWN':
            state = state.set('coord', [...state.get('coord'), action.payload ]);
            state = state.set('painting', true);
            state = state.set('clear', false);
            if(state.get('undo')) {
                state = state.set('undo', false);
                state = state.set('offset', state.get('offset').slice(0,-1)); // definitively remove last coords after undo and mousedown to draw again
            }

            return state;

        case 'MOUSEMOVE':
            return state.set('coord', [...state.get('coord'), action.payload ]);

        case 'MOUSEUP':
            state = state.set('painting', false);
            state = state.set('offset', [...state.get('offset'), state.get('coord') ]);
            state = state.set('coord', []);

            return state;

        case 'CLEAR':
            state = state.set('clear', true);
            state = state.set('coord', []);
            state = state.set('offset', []);

            return state;

        case 'UNDO':
            state = state.set('coord', []); // To assure last coord is empty after a redo
            state =  state.set('undo', true);

            return state;

        case 'REDO':
            state = state.set('undo', false);
            state = state.set('coord', state.get('offset')[state.get('offset').length-1]); //Fill coord with last coords drawn

            return state;

        default:
            return state;
    }
};

const settings = (state=initialSettings, action={}) => {
    switch(action.type) {
        case 'CHANGE_COLOR':
            return state.set('color', action.color);

        case 'CHANGE_SIZE':
            return state.set('size', action.size);

        case 'CHANGE_STYLE':
            return state.set('style', action.style);

        default:
            return state;
    }
};

// Create store
const store = createStore(combineReducers({draw, settings}));


// view
const canvasElem = document.getElementById('canvas');
const ctx = canvasElem.getContext("2d");

const clearCanvas = () => {
    ctx.clearRect(0, 0, ctx.canvas.width, ctx.canvas.height);
};

const drawStroke = (item, index, arr) => {
    ctx.strokeStyle = item.color;
    ctx.lineJoin = item.style;
    ctx.lineWidth = item.size;
    ctx.beginPath();
    if(index > 0 && item.drag) {
        ctx.moveTo(arr[index-1].x, arr[index-1].y);
    } else {
        ctx.moveTo(item.x-1, item.y);
    }

    ctx.lineTo(item.x, item.y);
    ctx.closePath();
    ctx.stroke();
};

const drawCanvas = (coord) => {
    coord.forEach(drawStroke);
};

const undo = (coordsArr) => {
    coordsArr.forEach((coords) => {
        coords.forEach(drawStroke);
    });
};


// render
const render = () => {
    const state = store.getState();

    if(state.draw.get('undo')) {
        clearCanvas();
        undo(state.draw.get('offset').slice(0,-1));
    } else {
        drawCanvas(state.draw.get('coord'));
    }

    if(state.draw.get('clear')) {
        clearCanvas();
    }

    // buttons state
    const buttons = document.getElementsByTagName('button');
    for(let i=0; i < buttons.length; i++) {
        buttons[i].removeAttribute('disabled');
        if(buttons[i].id === state.settings.get('color').id ||
            buttons[i].id === state.settings.get('size').id ||
            buttons[i].id === state.settings.get('style').id
        ){
            buttons[i].setAttribute('disabled', 'disabled');
        }
    }

    if(!state.draw.get('undo')) {
        document.getElementById('redo').setAttribute('disabled', 'disabled');
    }
};


render(store); // to render state of settings buttons at init

store.subscribe(render);

// Settings actions
const changeColorAction = (color) => {
    return {
        type: 'CHANGE_COLOR',
        color
    }
};

const changeSizeAction = (size) => {
    return {
        type: 'CHANGE_SIZE',
        size
    }
};

const changeStyleAction = (style) => {
    return {
        type: 'CHANGE_STYLE',
        style
    }
};

// Draw actions
const mousedownAction = (e) => {
    return {
        type: 'MOUSEDOWN',
        payload: {
            x: e.offsetX,
            y: e.offsetY,
            color: store.getState().settings.get('color').value,
            size: store.getState().settings.get('size').value,
            style: store.getState().settings.get('style').value
        }
    };
};

const mousemoveAction = (e) => {
    return {
        type: 'MOUSEMOVE',
        payload: {
            x: e.offsetX,
            y: e.offsetY,
            drag: true,
            color: store.getState().settings.get('color').value,
            size: store.getState().settings.get('size').value,
            style: store.getState().settings.get('style').value
        }
    };
};

// events
canvasElem.onmousedown = (e) => {
    store.dispatch(mousedownAction(e));
};

canvasElem.onmousemove = (e) => {
    if(store.getState().draw.get('painting')) {
        store.dispatch(mousemoveAction(e));
    }
};

canvasElem.onmouseup = () => {
    store.dispatch({type: 'MOUSEUP'});
};

// TODO: maybe refactor events handlers to have one. But I prefer one function by button.

//color
document.getElementById('black').onclick = (e) => {
    store.dispatch(changeColorAction({value:'#000000', id:'black'}));
};

document.getElementById('red').onclick = (e) => {
    store.dispatch(changeColorAction({value:'#FF0000', id:'red'}));
};

document.getElementById('blue').onclick = (e) => {
    store.dispatch(changeColorAction({value:'#0000FF', id:'blue'}));
};

// size
document.getElementById('thin').onclick = (e) => {
    store.dispatch(changeSizeAction({value:1, id:'thin'}));
};

document.getElementById('normal').onclick = (e) => {
    store.dispatch(changeSizeAction({value:5, id:'normal'}));
};

document.getElementById('large').onclick = (e) => {
    store.dispatch(changeSizeAction({value:10, id:'large'}));
};

// style
document.getElementById('bevel').onclick = (e) => {
    store.dispatch(changeStyleAction({value:'bevel', id:'bevel'}));
};

document.getElementById('round').onclick = (e) => {
    store.dispatch(changeStyleAction({value:'round', id:'round'}));
};

document.getElementById('miter').onclick = (e) => {
    store.dispatch(changeStyleAction({value:'miter', id:'miter'}));
};

document.getElementById('undo').onclick = (e) => {
    store.dispatch({type: 'UNDO'});
};

document.getElementById('redo').onclick = (e) => {
    store.dispatch({type: 'REDO'});
};

document.getElementById('clear').onclick = (e) => {
    store.dispatch({type: 'CLEAR'});
};



// FEW TESTING REDUCERS
expect(
    draw(undefined, {})
).toEqual(initialDraw);

expect(draw(
        initialDraw, {type: 'CLEAR'})
).toEqual(Map({
        color: '#000',
        painting:false,
        size: 5,
        offset: [],
        coord: [],
        clear: true,
        undo: false
    })
);

expect(
    draw(store.getState().draw, {type: 'MOUSEDOWN'}).get('clear')
).toEqual(false);

expect(
    settings(undefined, {}).get('style').value
).toEqual('round');

expect(
    settings(initialDraw, changeStyleAction({value:'miter', id:'miter'})).get('style').value
).toEqual('miter');

expect(
    settings(store.getState().settings, changeSizeAction({value:10, id:'large'})).get('size').value
).toEqual(10);


console.log('Tests passed!!');
