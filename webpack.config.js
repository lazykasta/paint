module.exports = {
    entry: './app.js',
    output: {
        path: __dirname,
        filename: './dist.js'
    },
    module: {
        loaders: [
            {
                test: /\.js$/,
                exclude: /node_modules/,
                loader: 'babel',
                query: { presets: [ 'es2015' ] }
            }
        ]
    }
};