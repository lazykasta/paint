# README #

After to clone repo, install dependencies and build app with **npm install**

### Some considerations ###
* This paint is done with redux as data model flow. We get data model consistent applying immutability to states.
* I've not get time enough to layout the original design. The number of color can be more just creating more buttons and passing to the action the hexa code color.
* Not compatible with ie for the canvas tag used in this challenge.